class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.integer :user_id
      t.string :user_name

      t.timestamps
    end
    add_index :posts, [:user_id, :created_at]
  end
end
