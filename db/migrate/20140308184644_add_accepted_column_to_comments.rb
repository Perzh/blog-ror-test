class AddAcceptedColumnToComments < ActiveRecord::Migration
  def change
    add_column :comments, :accepted, :boolean, default: false
  end
end
