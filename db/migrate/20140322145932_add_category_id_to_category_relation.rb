class AddCategoryIdToCategoryRelation < ActiveRecord::Migration
  def change
    add_column :category_relations, :category_id, :integer
  end
end
