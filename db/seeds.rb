# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Category.create(name: 'Games')
Category.create(name: 'Movies')
Category.create(name: 'Others')
Category.create(name: 'RPG')
Category.create(name: 'FPS')
Category.create(name: 'MOBA')
Category.create(name: 'DotA2')

#три корня
CategoryRelation.create(parent: 0, category_id: Category.find_by_name('Games').id)
CategoryRelation.create(parent: 0, category_id: Category.find_by_name('Movies').id)
CategoryRelation.create(parent: 0, category_id: Category.find_by_name('Others').id)

CategoryRelation.create(parent: Category.find_by_name('Games').id, category_id: Category.find_by_name('RPG').id)
CategoryRelation.create(parent: Category.find_by_name('Games').id, category_id: Category.find_by_name('FPS').id)
CategoryRelation.create(parent: Category.find_by_name('Games').id, category_id: Category.find_by_name('MOBA').id)

CategoryRelation.create(parent: Category.find_by_name('MOBA').id, category_id: Category.find_by_name('DotA2').id)