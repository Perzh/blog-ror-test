# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Blog::Application.initialize!

# Формат даты
Time::DATE_FORMATS[:ru_datetime] = "%Y.%m.%d"
