class UserMailer < ActionMailer::Base
  default from: "perzh@simpleblog.com"

  def welcome(user)
    @user = user
    @url = user_url(@user)
    mail(to: user.email, subject: 'Welcome, to Simple Blog!')
  end
end
