class User < ActiveRecord::Base
  has_many :posts, dependent: :destroy

  # перед сохранением модели в БД эмейл переходит в нижний регистр
  before_save { self.email = email.downcase }
  # Генерация уникального токена для сессии
  before_create :create_remember_me_token

  VALID_EMAL_REGEXP = /\A[a-z_]+([\w+\-.0-9_][a-z0-9_])*@[a-z\d\-.]+\.[a-z]+\z/i

  # эмейл существует, уникален (с точностью до регистра) и соответствует VALID_EMAL_REGEXP
  validates :email, presence: true, format: { with: VALID_EMAL_REGEXP }, uniqueness: { case_sensitive: false }

  has_secure_password

  # пароль существует и имееет длину от 6 до 12 символов
  validates :password, presence: true, length: { maximum: 12, minimum: 6 }

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def create_remember_me_token
      self.remember_me_token = User.encrypt(User.new_token)
    end
end
