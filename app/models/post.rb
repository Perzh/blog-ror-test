class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many :comments, dependent: :destroy
  has_and_belongs_to_many :tags, :uniq => true

  validates :user_id, presence: true
  validates :title, presence: true, length: { minimum: 1 }
  default_scope -> { order('posts.created_at DESC') }
end
