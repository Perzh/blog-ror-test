class Category < ActiveRecord::Base
  has_many :posts
  has_one :category_relation
end
