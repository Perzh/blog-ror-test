class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      #LOGIN
      login user
      redirect_back_or user
    else
      #Сгенирировать сообщение об ошибке и перерендерить форму входа
      flash.now[:error] = 'Invalid email or password'
      render 'new'
    end
  end

  def destroy
    logout
    redirect_to root_url
  end
end
