class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :signed_in_user, only: [:edit, :update, :destroy, :new, :create]
  before_action :admin_user, only: [:edit, :update, :destroy, :new, :create]

  # GET /posts
  # GET /posts.json
  def index
    @keyword = ''
    @posts = Post.paginate(page: params[:page], per_page: 5)
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    #новый комментарий
    @comment = @post.comments.build
    #Все комментарии к этому посту
    @comments = @post.comments.paginate(page: params[:page], per_page: 5)
  end

  # GET /posts/new
  def new
    @legend = 'New post:'
    @post = current_user.posts.build
  end

  # GET /posts/1/edit
  def edit
    @legend = 'Edit post:'
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = current_user.posts.build(post_params)

    set_tags(params[:post][:text])

    categoryName = params[:categories] || 'Others'
    @post.category = Category.find_by_name(categoryName)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    @post.tags.clear
    set_tags(params[:post][:text])

    categoryName = params[:categories] || 'Others'
    @post.category = Category.find_by_name(categoryName)

    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end

  def filter
    @tags = Tag.all

    @keyword = params[:keyword] || ''

    @selectedCategories = []
    if params[:categories]
      params[:categories].keys.each do |cat|
        @selectedCategories << Category.find_by_name(cat).id
      end
    end

    # выделяем массив тегов
    @selectedTags = []
    if params[:tag]
      params[:tag].each do |t|
        temp = Tag.find_by_text(t.downcase.strip)
        if temp
          @selectedTags << temp.id
        end
      end
    end

    key = '%' + @keyword.downcase + '%'
    # сначала фильтруем по ключевому слову, т.к. если оно пустое, фильтр выдаст список всех постов
    @posts = Post.where("content LIKE ? OR title LIKE ?", key, key)

    if params[:order]
      @posts = Post.where("content LIKE ? OR title LIKE ?", key, key).reverse_order
    end


    # фильтруем по тегам, если массив тегов не пуст
    if @selectedTags.count > 0
      @posts = @posts.includes(:tags).where("tags.id IN (?)",@selectedTags)
    end

    #фильтруем по категориям
    if @selectedCategories.count > 0
      @posts = @posts.where("posts.category_id IN (?)", @selectedCategories)
    end

    # в заключении пагинируем результат фильтрации
    @posts = @posts.paginate(page: params[:page], per_page: 5)

    render 'layouts/filter'
  end

  private
    def set_tags(tags_collection)
      tags_collection.split(',').each do |piece|
        next if piece.length > 15 && piece.length < 1
        piece.downcase!
        piece.strip!
        tg = Tag.find_by_text(piece)
        if tg
          tg.touch
        else
          tg = @post.tags.build(:text => piece)
          if tg.save
          else
            redirect_to @post, notice: 'Error while saving tag: ' + tg.text
          end
        end

        if !@post.tags.where('post_id = ? AND tag_id = ?', @post.id, tg.id).any?
          @post.tags << tg
        end
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:content, :title)
    end
end
