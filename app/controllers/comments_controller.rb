class CommentsController < ApplicationController

  def index
  end

  def new
    @comment = current_user.comments.build
  end

  def update
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:format])

    if signed_in? && current_user.admin?
      @comment.accepted = true
    else
      error = 'you must be administrator'
      format.html { redirect_to @post, notice: 'An error occured: ' + error }
    end

    if @comment.update(@comment.attributes)
      redirect_to @post, notice: 'Comment was successfully accepted.'
    else
      redirect_to @post, notice: 'An error occured whiles saving changes'
    end
  end

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)

    notification = 'Status: moderated.'

    if signed_in?
      @comment.user_id = current_user.id
      @comment.user_name = current_user.name

      if current_user.admin?
        @comment.accepted = true
      end
    end

    respond_to do |format|
      # пользователь должен быть либо залогинен либо заполнить капчу
      if (signed_in? || simple_captcha_valid?) && @comment.save
        format.html { redirect_to @post, notice: 'Comment was successfully submited. ' + notification }
      else
        format.html { redirect_to @post, notice: 'An error occured: ' }
      end
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:format])
    if @comment.destroy
      redirect_to @post, notice: 'Comment was successfully deleted.'
    else
      redirect_to @post, notice: 'An error occured while deleting comment'
    end
  end

  private

    def comment_params
      params.require(:comment).permit(:content, :user_name)
    end
end
