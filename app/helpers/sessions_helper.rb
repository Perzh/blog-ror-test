module SessionsHelper
  def store_location
    # если не get, то сохраняем печеньку
    session[:return_to] = request.url if request.get?
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    session.delete(:return_to)
  end

  def login(user)
    remember_me_token = User.new_token
    cookies.permanent[:remember_me_token] = remember_me_token
    user.update_attribute(:remember_me_token, User.encrypt(remember_me_token))
    self.current_user = user
  end

  def logout
    current_user.update_attribute(:remember_me_token, User.encrypt(User.new_token))
    cookies.delete(:remember_me_token)
    self.current_user = nil
  end

  def current_user?(user)
    user == current_user
  end

  def admin_user
    if !current_user.admin
      redirect_to root_url
    end
  end

  def signed_in_user
    unless signed_in?
      store_location
      redirect_to login_url
    end
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    remember_me_token = User.encrypt(cookies[:remember_me_token])
    @current_user ||= User.find_by(remember_me_token: remember_me_token)
  end

  def signed_in?
    !current_user.nil?
  end
end
